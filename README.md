## About me

* Fullstack and Software Developer
* Learned Python back in 2016 but began to code professionally in 2020
* Always learning a new language/framework

<br>

## Stack

<div>
<img alt="python" src="https://img.shields.io/badge/Python-3776AB?style=for-the-badge&logo=python&logoColor=yellow" />
<img alt="javascript" src="https://img.shields.io/badge/JavaScript-F7DF1E?style=for-the-badge&logo=javascript&logoColor=black" />
<img alt="rust" src="https://img.shields.io/badge/Rust-000000?style=for-the-badge&logo=rust&logoColor=orange" />
<img alt="django" src="https://img.shields.io/badge/Django-092E20?style=for-the-badge&logo=django&logoColor=white" />
<img alt="postgres" src="https://img.shields.io/badge/PostgreSQL-316192?style=for-the-badge&logo=postgresql&logoColor=white" />
<img alt="neo4j" src="https://img.shields.io/badge/Neo4j-000000?&style=for-the-badge&logo=neo4j&logoColor=yellow" />
</div>
<br>

## Where to find me

<div>
<a href="https://andrepz.medium.com/"><img alt="medium" src="https://img.shields.io/badge/medium-000000?logo=medium&logoColor=white&style=for-the-badge" /></a>
<a href="https://www.linkedin.com/in/andre-h-r-perez/"><img alt="linked-in" src="https://img.shields.io/badge/linkedin-%230077B5.svg?&style=for-the-badge&logo=linkedin&logoColor=white" /></a>
<a href="https://stackoverflow.com/users/14244437/pzvkn"><img alt="stack-overflow" src="https://img.shields.io/badge/stack%20overflow-FE7A16?logo=stack-overflow&logoColor=white&style=for-the-badge" /></a>
<a href="https://github.com/pzandre"><img alt="github" src="https://img.shields.io/badge/github-000000?logo=github&logoColor=white&style=for-the-badge" /></a>
</div>
<br>

## My Latest Medium Story
<div>
<a target="_blank" href="https://github-readme-medium-recent-article.vercel.app/medium/@andrepz/0"><img src="https://github-readme-medium-recent-article.vercel.app/medium/@andrepz/0" alt="Recent Article 0"> 
</div>
<br>

### PC Specs
<div>
<img src="https://img.shields.io/badge/AMD-Ryzen_5_3400G-000000?style=for-the-badge&logo=amd&logoColor=red" />
</br>
<img src="https://img.shields.io/badge/Corsair-16GB_3200MHz-000000?style=for-the-badge&logo=corsair&logoColor=yellow" />
</br>
<img src="https://img.shields.io/badge/Asus-B450_GAMING-000000?style=for-the-badge&logo=asus&logoColor=blue" />
</br>
<img src="https://img.shields.io/badge/WD-Black_NVMe_500GB-000000?style=for-the-badge" />
</br>
<img src="https://img.shields.io/badge/Pop!_OS-000000?style=for-the-badge&logo=popos&logoColor=ffffff" />
</br>
</div>
